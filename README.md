## moss-conductor

Orchestration module for parallel build queue implementations.
This module is intended for use within [bill](https://gitlab.com/serpent-os/core/bill/) and [avalanche](https://gitlab.com/serpent-os/infra/avalanche/).

### License

Copyright &copy; 2020-2022 Serpent OS Developers

`moss-conductor` is available under the terms of the [Zlib](https://gitlab.com/serpent-os/core/boulder/) license.